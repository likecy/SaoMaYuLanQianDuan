# vue-manage-system #
<a href="https://github.com/vuejs/vue">
    <img src="https://img.shields.io/badge/vue-2.6.10-brightgreen.svg" alt="vue">
  </a>
  <a href="https://github.com/ElemeFE/element">
    <img src="https://img.shields.io/badge/element--ui-2.8.2-brightgreen.svg" alt="element-ui">
  </a>
  <a href="https://github.com/lin-xin/vue-manage-system/blob/master/LICENSE">
    <img src="https://img.shields.io/github/license/mashape/apistatus.svg" alt="license">
  </a>
  <a href="https://github.com/lin-xin/vue-manage-system/releases">
    <img src="https://img.shields.io/github/release/lin-xin/vue-manage-system.svg" alt="GitHub release">
  </a>
  <a href="http://blog.gdfengshuo.com/example/work/#/donate">
    <img src="https://img.shields.io/badge/%24-donate-ff69b4.svg" alt="donate">
  </a>

基于Vue.js + Element UI 的后台管理系统解决方案。[线上地址](http:/flie.likecy.cn)

####封装百度echarts到组件

## 项目截图 ##
![Image text](https://gitee.com/likecy/SaoMaYuLanQianDuan/blob/master/pic/1.jpg)
![Image text](https://gitee.com/likecy/SaoMaYuLanQianDuan/blob/master/pic/2.jpg)
![Image text](https://gitee.com/likecy/SaoMaYuLanQianDuan/blob/master/pic/3.jpg)
![Image text](https://gitee.com/likecy/SaoMaYuLanQianDuan/blob/master/pic/4.jpg)


## 功能 ##
- [x] Element UI
- [x] 登录/注销
- [x] Dashboard
- [x] 文件表格
- [x] 分类选项卡
- [x] 图表 :echarts

## 安装步骤 ##
```
git clone https://gitee.com/likecy/SaoMaYuLanQianDuan.git      // 把模板下载到本地
cd XXXX    // 进入模板目录
npm install         // 安装项目依赖，等待安装完成之后，安装失败可用 cnpm 或 yarn

// 开启服务器，浏览器访问 http://localhost:8080
npm run serve

// 执行构建命令，生成的dist文件夹放在服务器下即可访问
npm run build
```